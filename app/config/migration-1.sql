ALTER TABLE ts_booking_bookings_slots ADD COLUMN `interval` VARCHAR(60);
ALTER TABLE ts_booking_bookings_slots ADD COLUMN `repeatUntil` date;
ALTER TABLE ts_booking_bookings_slots ADD COLUMN `repeats` int(10);